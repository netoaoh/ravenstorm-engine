workspace "Ravenstorm"
    architecture "x64"

    configurations
    {
        "Debug",
        "Release",
        "Dist"
    }

outputdir = "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"

project "ravenstorm"
    location "ravenstorm"
    kind "SharedLib"
    language "C++"

    targetdir("bin/" .. outputdir .. "/%{prj.name}")
    objdir("bin-int/" .. outputdir .. "/%{prj.name}")

    files
    {
        "%{prj.name}/src/**.h",
        "%{prj.name}/src/**.cpp"
    }

    includedirs
    {
        "%{prj.name}/src",
        "%{prj.name}/vendor/spdlog/include"
    }

    filter "system:windows"
        cppdialect "C++17"
        staticruntime "On"
        systemversion "latest"

        defines
        {
            "RS_PLATFORM_WINDOWS",
            "RS_BUILD_DLL"
        }

        postbuildcommands
        {
            ("{COPY} %{cfg.buildtarget.relpath} ../bin/" .. outputdir .. "/sandbox" )
        }

    filter "system:macosx"
        cppdialect "C++17"
        staticruntime "On"
        systemversion "latest"

        defines
        {
            "RS_PLATFORM_MACOSX"
        }

    filter "configurations:Debug"
        defines "RS_DEBUG"
        symbols "On"
    
    filter "configurations:Release"
        defines "RS_RELEASE"
        optimize "On"

    filter "configurations:Dist"
        defines "RS_DIST"
        optimize "On"

project "sandbox"
    location "sandbox"
    kind "ConsoleApp"
    language "C++"

    targetdir("bin/" .. outputdir .. "/%{prj.name}")
    objdir("bin-int/" .. outputdir .. "/%{prj.name}")

    files
    {
        "%{prj.name}/src/**.h",
        "%{prj.name}/src/**.cpp"
    }

    includedirs
    {
        "ravenstorm/vendor/spdlog/include",
        "ravenstorm/src"
    }

    links
    {
        "ravenstorm"
    }

    filter "system:windows"
        cppdialect "C++17"
        staticruntime "On"
        systemversion "latest"

        defines
        {
            "RS_PLATFORM_WINDOWS"
        }

    filter "system:macosx"
        cppdialect "C++17"
        staticruntime "On"
        systemversion "latest"

        defines
        {
            "RS_PLATFORM_MACOSX"
        }

    filter "configurations:Debug"
        defines "RS_DEBUG"
        symbols "On"
    
    filter "configurations:Release"
        defines "RS_RELEASE"
        optimize "On"

    filter "configurations:Dist"
        defines "RS_DIST"
        optimize "On"