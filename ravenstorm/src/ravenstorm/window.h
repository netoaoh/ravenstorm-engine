#ifndef WINDOW_H
#define WINDOW_H

#include "ravenstorm.h"

#include "ravenstorm/core.h"
#include "ravenstorm/events/event.h"

namespace Ravenstorm
{
    struct WindowProps
    {
        std::string Title;
        unsigned int Width;
        unsigned int Height;

        WindowProps(const std::string& title = "Ravenstorm Engine", unsigned int width = 800, unsigned int height = 600)
            : Title(title), Width(width), Height(height)
        {}
    };

    //Interface representing a desktop system based Window
    class RS_API Window
    {
    public:
        using EventCallbackFn = std::function<void(Event&)>;

        virtual ~Window(){}

        virtual void OnUpdate() = 0;

        virtual unsigned int GetWidth() const = 0;
        virtual unsigned int GetHeight() const = 0;

        //Window attributes
        virtual void SetEventCallback(const EventCallbackFn& callback) = 0;
        virtual void SetVSync(bool enabled) = 0;
        virtual bool IsVSync() const = 0;

        static Window* Create(const WindowProps& props = WindowProps());
    };
}

#endif