#ifndef ENTRYPOINT_H
#define ENTRYPOINT_H

//TODO: Separate entrypoint per platform

extern Ravenstorm::Application* Ravenstorm::CreateAplication();

int main(int argc, char** argv)
{
    std::cout << "Starting Ravenstorm Engine..." << std::endl;

    Ravenstorm::Log::Init();
    RS_CORE_WARN("Initialized Log!");
    int a = 5;
    RS_INFO("Hello! Var={0}", a);

    auto app = Ravenstorm::CreateAplication();
    app->Run();

    std::cout << "Finishing Ravenstorm Engine..." << std::endl;

    delete app;

    return 0;
}

#endif