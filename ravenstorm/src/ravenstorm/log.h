#ifndef LOG_H
#define LOG_H

#include <memory>

#include "core.h"
#include "spdlog/spdlog.h"
#include "spdlog/sinks/stdout_color_sinks.h"
#include "spdlog/fmt/ostr.h"

namespace Ravenstorm
{
    class RS_API Log
    {
    public:
        static void Init();

        inline static std::shared_ptr<spdlog::logger>& GetCoreLogger() { return s_CoreLogger; }
        inline static std::shared_ptr<spdlog::logger>& GetClientLogger() { return s_ClientLogger; }

    private:
        static std::shared_ptr<spdlog::logger> s_CoreLogger;
        static std::shared_ptr<spdlog::logger> s_ClientLogger;
    };
}

// Core log macros
#define RS_CORE_TRACE(...) ::Ravenstorm::Log::GetCoreLogger()->trace(__VA_ARGS__)
#define RS_CORE_INFO(...) ::Ravenstorm::Log::GetCoreLogger()->info(__VA_ARGS__)
#define RS_CORE_WARN(...) ::Ravenstorm::Log::GetCoreLogger()->warn(__VA_ARGS__)
#define RS_CORE_ERROR(...) ::Ravenstorm::Log::GetCoreLogger()->error(__VA_ARGS__)
#define RS_CORE_FATAL(...) ::Ravenstorm::Log::GetCoreLogger()->fatal(__VA_ARGS__)

// Client log macros
#define RS_TRACE(...) ::Ravenstorm::Log::GetClientLogger()->trace(__VA_ARGS__)
#define RS_INFO(...) ::Ravenstorm::Log::GetClientLogger()->info(__VA_ARGS__)
#define RS_WARN(...) ::Ravenstorm::Log::GetClientLogger()->warn(__VA_ARGS__)
#define RS_ERROR(...) ::Ravenstorm::Log::GetClientLogger()->error(__VA_ARGS__)
#define RS_FATAL(...) ::Ravenstorm::Log::GetClientLogger()->fatal(__VA_ARGS__)



#endif