#ifndef CORE_H
#define CORE_H

    #ifdef RS_PLATFORM_WINDOWS
        #ifdef RS_BUILD_DLL
            #define RS_API __declspec(dllexport)
        #else
            #define RS_API __declspec(dllimport)
        #endif
    #else
        #define RS_API
    #endif

    #define BIT(x) (1 << x)

#endif