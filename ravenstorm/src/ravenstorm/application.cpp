#include "application.h"

#include "ravenstorm/events/applicationevent.h"
#include "ravenstorm/log.h"

namespace Ravenstorm
{
    Application::Application()
    {

    }

    Application::~Application()
    {

    }

    void Application::Run()
    {
        WindowResizeEvent e(1280, 720);
		if (e.IsInCategory(EventCategoryApplication))
		{
			RS_TRACE(e);
		}
		if (e.IsInCategory(EventCategoryInput))
		{
			RS_TRACE(e);
		}

		while (true);
    }
}