#ifndef APPLICATION_H
#define APPLICATION_H

#include "ravenstorm/core.h"

namespace Ravenstorm
{
    class RS_API Application
    {
    public:
        Application();
        virtual ~Application();

        void Run();
    };

    Application* CreateAplication();
}

#endif